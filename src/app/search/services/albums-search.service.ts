import { Injectable, Inject, EventEmitter } from '@angular/core';
import { SEARCH_CONFIG, SearchConfig } from './tokens';
import { PagingObject, Album, ArtistsSearchResponse, AlbumsSearchResponse } from 'src/app/core/model/search';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'src/app/core/security/auth.service';
// import { SearchModule } from '../search.module';
import { map, pluck, catchError, startWith, mergeAll, mergeMap, exhaustMap, switchMap, exhaust, switchAll, debounceTime, distinctUntilChanged, skip } from 'rxjs/operators'
import { EMPTY, NEVER, throwError, timer, from, of, merge, concat, Subject, ReplaySubject, BehaviorSubject, observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
  // providedIn: SearchModule
})
export class AlbumsSearchService {

  results: Partial<PagingObject<Partial<Album>>> = {
    items: [
     /*  {
        id: '123', name: 'Test from service',
        images: [{
          url: 'https://www.placecage.com/c/300/300', height: 100, width: 100
        }]
      },
      {
        id: '123', name: 'Test 123',
        images: [{
          url: 'https://www.placecage.com/c/400/400', height: 100, width: 100
        }]
      },
      {
        id: '123', name: 'Test 234',
        images: [{
          url: 'https://www.placecage.com/c/300/300', height: 100, width: 100
        }]
      },
      {
        id: '123', name: 'Test 234',
        images: [{
          url: 'https://www.placecage.com/c/300/300', height: 100, width: 100
        }]
      }, */
    ]
  }

  private notifications = new ReplaySubject(1, 5000)
  private albums = new BehaviorSubject<PagingObject<Album>>(//
    this.results as PagingObject<Album>
  )
  private query = new BehaviorSubject<string>('')

  /* Outputs */
  public notificationChanges = this.notifications.asObservable()
  public albumChanges = this.albums.asObservable()
  public queryChanges = this.query.asObservable()

  constructor(
    private http: HttpClient,
    @Inject(SEARCH_CONFIG) private config: SearchConfig) {

    (window as any).subject = this.albums

    this.query
      .pipe(
        skip(1),
        // debounceTime(400),
        // distinctUntilChanged(),
        map(query => ({ type: 'album', query })),
        switchMap(params => this.fetchResults(params)),
        map(resp => resp.albums),
      )
      .subscribe(this.albums)
    // .subscribe({
    //   next: resp => this.albums.next(resp)
    // })
  }

  fetchResults(params: { type: string; query: string; }) {
    return this.http.get<AlbumsSearchResponse>(//
      this.config.api_url + this.config.endpoint, {
      params
    })
      .pipe(catchError(err => {
        this.notifications.next(err.message);
        return [];
      }));
  }

  /* Input */
  search(query = 'batman') {
    this.query.next(query)
  }

}
