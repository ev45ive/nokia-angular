import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-selected-playlist-form',
  templateUrl: './selected-playlist-form.component.html',
  styleUrls: ['./selected-playlist-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectedPlaylistFormComponent implements OnInit {

  selected = this.playlistsService.selectedChanges

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService) {
  }

  ngOnInit(): void {
  }

  cancel() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }

  save(draft: Playlist) {

  }

}
