import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTextMessageComponent } from './router-text-message.component';

describe('RouterTextMessageComponent', () => {
  let component: RouterTextMessageComponent;
  let fixture: ComponentFixture<RouterTextMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouterTextMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterTextMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
