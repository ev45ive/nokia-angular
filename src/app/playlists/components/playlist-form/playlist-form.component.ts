import { Component, OnInit, Input, OnChanges, SimpleChanges, AfterViewInit, AfterViewChecked, EventEmitter, Output, ViewChild } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit, OnChanges, AfterViewInit, AfterViewChecked {

  @Input()
  playlist!: Playlist

  constructor() { }

  @Output() cancel = new EventEmitter();
  @Output() savePlaylist = new EventEmitter<Playlist>();

  save(formRef: NgForm) {
    // console.log(formRef)

    const draft = {
      ...this.playlist,
      ...formRef.value
    }

    this.savePlaylist.emit(draft)
  }

  // cancel(){
  //   confirm('You have unsaved changes. Are you sure?')
  // }

  ngOnChanges(changes: SimpleChanges) {
    // this.draft = { ...changes['playlist'].currentValue }
  }

  ngOnInit(): void {
  }

  // @ViewChild('formRef', { read: NgForm/* , static: false  */ })
  @ViewChild(NgForm)
  formRef?: NgForm

  ngAfterViewInit(): void {
    setTimeout(() => {
      console.log(this.formRef?.value)
      // this.formRef?.setValue(this.playlist)
      this.formRef?.form.patchValue(this.playlist)
    })
  }

  ngAfterViewChecked(): void { }
}

