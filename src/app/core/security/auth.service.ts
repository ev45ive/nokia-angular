import { Injectable, InjectionToken, Inject } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

export const AuthConfigToken = new InjectionToken<AuthConfig>('Auth Config')

export interface AuthConfig {
  url: string,
  client_id: string,
  response_type: string,
  redirect_uri: string,
  // state
  /**
   * A space-separated list of scopes: see Using Scopes.
   */
  scopes: string[],
  show_dialog: boolean
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token = new BehaviorSubject<string | null>(null)
  public tokenChange = this.token.asObservable()
  public isLoggedIn = this.tokenChange.pipe(map(token => !!token))

  constructor(@Inject(AuthConfigToken) private config: AuthConfig) { }

  init() {
    if (!this.token.getValue()) {
      const json = window.sessionStorage.getItem('token')
      this.token.next(json && JSON.parse(json))
    }

    if (!this.token.getValue() && window.location.hash) {
      const p = new HttpParams({ fromString: window.location.hash })
      this.token.next(p.get('#access_token'))
      // this.expires = p.get('expires')
      window.sessionStorage.setItem('token', JSON.stringify(this.token.getValue()))
    }

    if (!this.token.getValue()) {
      this.authorize()
    }
  }

  authorize() {
    window.sessionStorage.removeItem('token')

    const { url, client_id, redirect_uri, response_type, scopes, show_dialog } = this.config;

    const p = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type,
        // https://www.google.com/search?q[]=dogbert&q[]=dogbert&q[]=dogbert
        scope: scopes.join(' '),
        show_dialog: show_dialog ? 'true' : 'false'
      }
    })

    window.location.href = `${url}?${p.toString()}`
    // window.open(`${url}?${p.toString()}`,'_blank','width=200')
  }


  getToken() {
    if (!this.token.getValue()) { this.authorize() }
    return this.token.getValue()
  }
}
