import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SEARCH_CONFIG, SearchConfig } from 'src/app/search/services/tokens';
import { BehaviorSubject } from 'rxjs';
import { UserProfile } from '../model/UserProfile';
import { AuthService } from '../security/auth.service';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user = new BehaviorSubject<UserProfile | null>(null)
  public userChange = this.user.asObservable()
  public currentUser: UserProfile | null = null

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    @Inject(SEARCH_CONFIG) private config: SearchConfig) {

    this.auth.isLoggedIn.pipe(filter(loggedIn => loggedIn === true)).subscribe(() => {
      this.fetchCurrentUser()
    })
  }

  fetchCurrentUser() {
    this.http.get<UserProfile>(`${this.config.api_url}me`)
      .subscribe(userProfile => {
        this.user.next(userProfile);
        this.currentUser = userProfile
      })
  }
}
