import { Directive, TemplateRef, ViewContainerRef, Input, ComponentFactoryResolver } from '@angular/core';
import { PlaylistsComponent } from 'src/app/playlists/views/playlists/playlists.component';
import { PlaylistListComponent } from 'src/app/playlists/components/playlist-list/playlist-list.component';

type Ctx = {
  $implicit: any
}

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {

  @Input('appUnlessLubie')
  set hide(hide: boolean) {
    if (hide) {
      this.vcr.clear()
    } else {
      this.vcr.createEmbeddedView(this.tpl, {
        $implicit: 'Placki'
      }, 0)

      const factory = this.resolver.resolveComponentFactory(
        PlaylistListComponent
      )

      const c = this.vcr.createComponent(factory, 0)

      c.instance.playlists = [{name:'test'}] as any
      c.instance.selectedChange.subscribe(console.log)
    }
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private tpl: TemplateRef<Ctx>,
    private vcr: ViewContainerRef
  ) {
    // tpl.createEmbeddedView()


  }

}
