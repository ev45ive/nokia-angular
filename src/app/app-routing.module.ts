import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './core/view/page-not-found/page-not-found.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full'
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module')
      .then(m => m.SearchModule)
  },
  {
    path: 'playlists',
    loadChildren: () => import('./playlists/playlists.module')
      .then(m => m.PlaylistsModule)
  },
  // {
  //   path: 'micro',
  //   loadChildren: () => import('micro-front').then(m => m.MicroFrontModule)
  // },
  {
    path: '**',
    component: PageNotFoundComponent,
    // redirectTo: 'playlists',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // useHash: true,
    enableTracing: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
