import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from '../security/auth.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(
    private auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const authReq = req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    })

    return next.handle(authReq).pipe(
      catchError((error, caught) => {
        if (!(error instanceof HttpErrorResponse)) {
          return throwError(error)
        }

        if (error.status == 401) {
          this.auth.authorize()
        }

        return throwError(error.error.error)

        // return caught // retry same subscription
        // return []
        // return EMPTY
        // return NEVER
      })
    )
  }
}
/*
http.get(..)
A.next = B
B.next = C
C.next = HttpCLientBackend */