import { NgModule } from '@angular/core';
import { MicroFrontComponent } from './micro-front.component';
import { AppRoutingModule } from './app-routing/app-routing.module';



@NgModule({
  declarations: [MicroFrontComponent],
  imports: [
    AppRoutingModule
  ],
  exports: [MicroFrontComponent]
})
export class MicroFrontModule { }
