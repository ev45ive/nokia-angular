import { AuthConfig } from 'src/app/core/security/auth.service';

export const environment = {
  production: true,

  search: {
    api_url: 'https://api.spotify.com/v1/',
    endpoint: 'search'
  },

  auth: {
    url: 'https://accounts.spotify.com/authorize',
    client_id: 'c456a7ab04eb44a5a9ab8d7f059e74bb',
    response_type: 'token',
    redirect_uri: 'http://localhost:4200/',
    scopes: [
      'playlist-read-public',
      'playlist-read-private',
      'playlist-modify-private',
    ],
    show_dialog: true
  } as AuthConfig
};
