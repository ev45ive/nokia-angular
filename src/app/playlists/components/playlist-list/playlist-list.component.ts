import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';
import { NgForOf, NgForOfContext } from '@angular/common';

NgForOf
NgForOfContext
// $implicit

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  // inputs:['playlists:value']
  // encapsulation: ViewEncapsulation.ShadowDom
  // encapsulation: ViewEncapsulation.Emulated
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistListComponent implements OnInit {

  // @Input('playlists')
  @Input()
  playlists: Playlist[] = []

  @Input()
  selected?: Playlist;

  hover?: Playlist['id']

  @Output() selectedChange = new EventEmitter<Playlist>();

  select(p: Playlist) {
    this.selected = p.id == this.selected?.id ? undefined : p
    this.selectedChange.emit(this.selected)
  }

  trackFn(index: number, item: Playlist) {
    return item.id
  }

  constructor() { }

  ngOnInit(): void {
  }

}
