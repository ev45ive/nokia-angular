import { Injectable, Inject } from '@angular/core';
import { Playlist } from '../model/playlist';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SEARCH_CONFIG, SearchConfig } from 'src/app/search/services/tokens';
import { UserService } from './user.service';
import { PagingObject } from '../model/search';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {
  createPlaylist(draft: Partial<Playlist>) {

    delete draft.favorite;
    delete draft.color

    this.http.post(
      this.config.api_url + `users/${this.user.currentUser?.id}/playlists`
      , draft, {}).subscribe(resp => {
        debugger
      })
  }

  private playlists = new BehaviorSubject<Playlist[]>([
    /*  {
       id: '123',
       name: 'Angular Hits',
       favorite: true,
       color: '#ff00ff',
     },
     {
       id: '234',
       name: 'Angular Top20',
       favorite: true,
       color: '#ffff00',
     },
     {
       id: '345',
       name: 'Best of angular',
       favorite: false,
       color: '#00ff00',
     }, */
  ])
  public playlistsChanges = this.playlists.asObservable()

  private selected = new BehaviorSubject<Playlist | null>(null)
  public selectedChanges = this.selected.asObservable()

  constructor(
    private http: HttpClient,
    private user: UserService,
    @Inject(SEARCH_CONFIG) private config: SearchConfig) { }

  fetchMyPlaylists() {
    this.http.get<PagingObject<Playlist>>(`${this.config.api_url}me/playlists`)
      .subscribe(rsp => {
        this.playlists.next(rsp.items)
      })
  }

  selectPlaylistById(playlistId: string | number | null) {

    setTimeout(() => {
      const playlist = this.playlists.getValue().find(p => p.id === playlistId)
      this.selected.next(playlist || null)
    }, 1000)
  }

}
