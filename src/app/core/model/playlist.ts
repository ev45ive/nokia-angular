// export class Playlist {
//   constructor(
//     public id: number,
//     public name: string
//   ) { }
// }
// new Playlist(resp.id, resp.name, ...)


export interface Entity {
  id: string | number
  name: string
}

export interface Track extends Entity { }

/**
 * Playlist model
 */
export interface Playlist extends Entity {
  favorite: boolean
  public?:boolean
  /**
   * HEX color string
   * `#ff00ff`
   */
  color: string
  description: string
  // tracks: Array<Track>
  tracks?: Track[] 
}

// const playlist: Playlist = {
//   id: '123',
//   name: 'Test',
//   color: '#ff00ff',
//   favorite: true,
//   tracks: []
// }

// playlist.tracks.slice()

// if(playlist.tracks && playlist.tracks.length){}
// playlist.tracks && playlist.tracks.slice()

// playlist.tracks?.slice()

// if('string' === typeof playlist.id){
//   playlist.id.slice()
// }else{
//   playlist.id.toFixed()
// }

// interface Point { x: number, y: number }
// interface Vector { x: number, y: number, length: number }

// let p: Point = { x: 123, y: 213 }
// let v: Vector = { x: 123, y: 213, length: 123 }

// p = v
// v = p