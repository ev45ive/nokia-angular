import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistsComponent } from './views/playlists/playlists.component';
import { SelectedPlaylistDetailsComponent } from './containers/selected-playlist-details/selected-playlist-details.component';
import { SelectedPlaylistFormComponent } from './containers/selected-playlist-form/selected-playlist-form.component';
import { RouterTextMessageComponent } from './containers/router-text-message/router-text-message.component';
import { CreatePlaylistComponent } from './containers/create-playlist/create-playlist.component';


const routes: Routes = [
  {
    path: '',
    component: PlaylistsComponent,
    children: [
      {
        path: '',
        component: RouterTextMessageComponent,
        data: {
          message: 'Please select playlist'
        }
      },
      {
        path: 'create',
        component: CreatePlaylistComponent
      },
    ]
  },
  {
    path: ':playlist_id',
    component: PlaylistsComponent,
    children: [
      {
        path: '',
        component: SelectedPlaylistDetailsComponent
      },
      {
        path: 'edit',
        component: SelectedPlaylistFormComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
