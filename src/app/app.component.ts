import { Component, ChangeDetectorRef, NgZone, ChangeDetectionStrategy, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root, .placki',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements AfterViewChecked {
  title = 'nokia-angular';
  hide = false

  // After children checked
  ngAfterViewChecked() {

    // ExpressionChangedAfterItHasBeenCheckedError
    // this.title = 'placki'

    // Schedule for next change detection
    // setTimeout(() => {
    //   this.title = 'Music App'
    // })
  }


  constructor(private router: Router) { }

  search(query: string) {
    this.router.navigate(['/search'], { queryParams: { query } })
  }

}
