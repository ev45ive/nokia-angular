import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './views/playlists/playlists.component';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { PlaylistItemComponent } from './components/playlist-item/playlist-item.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistFormComponent } from './components/playlist-form/playlist-form.component';
import { SharedModule } from '../shared/shared.module';
import { SelectedPlaylistDetailsComponent } from './containers/selected-playlist-details/selected-playlist-details.component';
import { SelectedPlaylistFormComponent } from './containers/selected-playlist-form/selected-playlist-form.component';
import { RouterTextMessageComponent } from './containers/router-text-message/router-text-message.component';
import { CreatePlaylistComponent } from './containers/create-playlist/create-playlist.component';
@NgModule({
  declarations: [
    PlaylistsComponent, 
    PlaylistListComponent, 
    PlaylistItemComponent, 
    PlaylistDetailsComponent, 
    PlaylistFormComponent, SelectedPlaylistDetailsComponent, SelectedPlaylistFormComponent, RouterTextMessageComponent, CreatePlaylistComponent
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    SharedModule
  ],
  // exports:[
  //   PlaylistsComponent
  // ]
})
export class PlaylistsModule { }
