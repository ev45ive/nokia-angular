import { Component, OnInit, Inject } from '@angular/core';
import { PagingObject, Album, Image, AlbumsSearchResponse } from 'src/app/core/model/search';
import { AlbumsSearchService } from '../../services/albums-search.service';
import { Subscription, Subject, Observable, ConnectableObservable, ReplaySubject } from 'rxjs';
import { tap, takeUntil, multicast, refCount, publishReplay, share, shareReplay, publish } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

// type id = string | number | undefined

// type Partial<T> = {
//   [K in keyof T]?:  T[K]
// }


@Component({
  selector: 'app-albums-search',
  templateUrl: './albums-search.component.html',
  styleUrls: ['./albums-search.component.scss'],
})
export class AlbumsSearchComponent implements OnInit {

  notificationChanges = this.searchService.notificationChanges
  queryChanges = this.searchService.queryChanges

  albumChanges = this.searchService.albumChanges.pipe(
    shareReplay()
  )

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private searchService: AlbumsSearchService
  ) { }

  search(query: string) {
    this.router.navigate([/* '/search' */], {
      queryParams: {
        query
      },
      relativeTo: this.route,
      replaceUrl: true,
      state: { 'placki': true }
    })
  }

  ngOnInit(): void {
    // const query = this.route.snapshot.queryParamMap.get('query')
    this.route.queryParamMap.subscribe(queryParamMap => {
      const query = queryParamMap.get('query')
      this.searchService.search(query || 'batman')
      // this.search(query || 'batman')
    })
  }
}
