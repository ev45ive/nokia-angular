import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-router-text-message',
  templateUrl: './router-text-message.component.html',
  styleUrls: ['./router-text-message.component.scss']
})
export class RouterTextMessageComponent implements OnInit {

  data = this.route.data

  constructor(
    private route: ActivatedRoute

  ) { }

  ngOnInit(): void {
  }

}
