import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-micro-front',
  template: `
    <p>
      micro-front works!
    </p>
  `,
  styles: [
    `
      :host {
        display: block;
      }
    `
  ]
})
export class MicroFrontComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
