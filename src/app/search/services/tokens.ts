import { InjectionToken } from '@angular/core';
export interface SearchConfig {
  api_url: string;
  endpoint: string
}
export const SEARCH_CONFIG = new InjectionToken<SearchConfig>(//
  'SEARCH_CONFIG for AlbumsSearchService');
