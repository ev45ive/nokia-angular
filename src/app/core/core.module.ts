import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthConfigToken, AuthConfig, AuthService } from './security/auth.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { UserService } from './services/user.service';
import { SEARCH_CONFIG, SearchConfig } from '../search/services/tokens';
import { environment } from 'src/environments/environment';
import { PageNotFoundComponent } from './view/page-not-found/page-not-found.component';


@NgModule({
  declarations: [PageNotFoundComponent],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
  ]
})
export class CoreModule {

  constructor(private auth: AuthService, private user: UserService) {
    this.auth.init()
  }

  /**
   * Core config
   * @param auth AuthConfig from environment 
   */
  static forRoot(auth: AuthConfig, api: SearchConfig): ModuleWithProviders {

    return {
      ngModule: CoreModule,
      providers: [
        {
          provide: AuthConfigToken,
          useValue: auth
        },

        {
          provide: SEARCH_CONFIG,
          useValue: api
        },
      ]
    }
  }

  // static forChild(){}
}
