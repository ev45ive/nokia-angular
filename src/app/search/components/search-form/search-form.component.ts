import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, ValidatorFn, Validators, ValidationErrors, AbstractControl, Validator, AsyncValidator, AsyncValidatorFn } from '@angular/forms';
import { filter, debounceTime, distinctUntilChanged, map, withLatestFrom, mapTo } from 'rxjs/operators';
import { Observable, Subscriber, combineLatest, pipe } from 'rxjs';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Output() search = new EventEmitter<string>();

  @Input() set query(q: string) {
    (this.queryForm.get('query') as FormControl).setValue(q, {
      emitEvent: false,
      // onlySelf:false
    })
  }

  // https://rxjs-dev.firebaseapp.com/api/index/function/bindCallback
  // return debeounce() this.http.post(...).pipe(map(resp => resp.errors || null ))
  asyncCensor: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
    // console.log('> someone just created observable!' + control.value)

    return new Observable((subscriber: Subscriber<ValidationErrors | null>) => {
      // someone just subscribed!

      const handler = setTimeout(() => {
        const badword = 'batman'
        const hasError = ('' + control.value).includes(badword)
        // console.log('>> got validation')

        subscriber.next(hasError ? { censor: { badword } } : null)
        // subscriber.error('placki')
        subscriber.complete()
      }, 2000)

      return /* TeardownLogic */() => {
        // console.log('<< someone just unsubscribed!')
        clearTimeout(handler)
      }
    })
  }

  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    // control.parent.get('sibling')
    const badword = 'batman'
    const hasError = ('' + control.value).includes(badword)

    return hasError ? {
      censor: { badword },
    } : null
  }

  queryForm = this.fb.group({
    'query': new FormControl('', [
      // this.censor,
      Validators.required,
      Validators.minLength(3)
    ], [
      this.asyncCensor
    ]),
    'type': ['album', {
      updateOn: 'blur', // emit ValueChanges,
      validators: []
    }]
  }, [/* groupValidator */])

  constructor(private fb: FormBuilder) {
    (window as any).form = (this.queryForm)

    const statusChanges = this.queryForm.get('query')!.statusChanges;
    const valueChanges = this.queryForm.get('query')!.valueChanges;

    function createFormBlocker(valueChanges: Observable<any>) {
      return pipe(
        filter(status => status === 'VALID'),
        mapTo(true),
        withLatestFrom(valueChanges),
        map(([valid, value]) => value),
      )
    }

    const searchChanges = statusChanges.pipe(
      createFormBlocker(valueChanges),
    )

    searchChanges.subscribe(this.search)
  }

  sendQuery() {
    this.search.emit(this.queryForm.get('query')?.value)
  }


  ngOnInit(): void { }

}
