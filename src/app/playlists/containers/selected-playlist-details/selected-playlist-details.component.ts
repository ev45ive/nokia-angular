import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { Router, ActivatedRoute } from '@angular/router';
import { throwIfEmpty } from 'rxjs/operators';

@Component({
  selector: 'app-selected-playlist-details',
  templateUrl: './selected-playlist-details.component.html',
  styleUrls: ['./selected-playlist-details.component.scss']
})
export class SelectedPlaylistDetailsComponent implements OnInit {

  selected = this.playlistsService.selectedChanges

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService) {
  }

  ngOnInit(): void {
  }

  edit() {
    this.router.navigate(['edit'], { relativeTo: this.route })
  }

}
