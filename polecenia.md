# Clone

git clone https://bitbucket.org/ev45ive/nokia-angular.git
cd nokia-angular
npm i
ng s -o

# Fetch changes

git stash
git pull -f

# Instalations
node -v
npm -v
code -v
git --version

# Angular CLI

npm i -g @angular/cli

ng --version

# Generate new project

ng new nokia-angular

? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS [ https://sass-lang.com/documentation/syntax#scss

# Serve development server

ng s -o

# Custom webpack

https://www.digitalocean.com/community/tutorials/angular-custom-webpack-config

# VSCODE

https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
https://prettier.io/docs/en/integrating-with-linters.html
https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console
https://marketplace.visualstudio.com/items?itemName=infinity1207.angular2-switcher
https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline

# Generating components and modules

LIFT guidelines

- Locating our code is easy
- Identify code at a glance
- Flat structure as long as we can
- Try to stay DRY (Don’t Repeat Yourself) or T-DRY

# Always loaded

ng g m core -m app

<!-- ng g m core -m app --force -->

## Shared

ng g m shared -m app

## Optional feature modules

ng g m playlists --routing -m app
ng g m search --routing -m app

# Views, Containers, Presenters

<!-- ng g c playlists/containers/my-playlsits
ng g c playlists/containers/user-playlsits
ng g c playlists/containers/top-playlsits
ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-grid -->

ng g c playlists/views/playlists
ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form


# MOdel

ng g interface core/model/playlist


# Debugging
ng.getContext($0)
ng.getComponent($0)
ng.applyChanges(ng.getContext($0))

<pre>{{ playlist | json }}</pre>

# Pipes
<pre>{{ playlist | json }}</pre>

ng g p shared/common/yesno --export

# Directives
ng g d shared/common/highlight --export


# Music Search
<!-- ng g m search --routing -m app -->
ng g c search/views/albums-search

ng g c search/components/search-form
ng g c search/components/search-results
ng g c search/components/album-card

ng g s search/services/albums-search

# RxJS
https://rxmarbles.com/
https://rxjs-dev.firebaseapp.com/


# Nrvl NX
https://nx.dev/react/cli/overview
https://semver.org/
https://medium.com/@angularlicious/lazy-load-angular-library-micro-applications-648c947821c3

# Ankieta
http://tiny.cc/8ptapz

k.stepien@sages.com.pl 

https://www.linkedin.com/in/mateuszkulesza/

