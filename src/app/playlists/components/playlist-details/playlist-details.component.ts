import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist!: Playlist
  
  /*  = {
    id: '123',
    name: 'Test',
    favorite: true,
    color: '#ff00ff',
    tracks: []
  } */

  constructor() { }

  ngOnInit(): void {
  }

}
