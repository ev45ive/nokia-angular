import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { AlbumsSearchComponent } from './views/albums-search/albums-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { environment } from 'src/environments/environment';
import { SEARCH_CONFIG } from "./services/tokens";
import { HttpClientModule } from '@angular/common/http'
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SyncSearchComponent } from './views/sync-search/sync-search.component';
// import { HttpClientTestingModule} from '@angular/common/http/testing'

@NgModule({
  declarations: [
    AlbumsSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    SyncSearchComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: SEARCH_CONFIG,
      useValue: environment.search
    },
    // {
    //   provide: AlbumsSearchService,
    //   useFactory(config: SearchConfig) {
    //     return new AlbumsSearchService(config)
    //   },
    //   deps: [SEARCH_CONFIG]
    // },
    // {
    //   provide: AbstractSearchService,
    //   useClass: AlbumsSearchService,
    //   // deps: [SEARCH_CONFIG_OVERRIDE]
    // },
    // {
    //   provide:AlbumsSearchService,
    //   useClass:AlbumsSearchService
    // },
    // AlbumsSearchService,
  ]
})
export class SearchModule { }
