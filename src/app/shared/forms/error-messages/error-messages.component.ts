import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-error-messages',
  templateUrl: './error-messages.component.html',
  styleUrls: ['./error-messages.component.scss']
})
export class ErrorMessagesComponent implements OnInit {

  @Input()
  field?: AbstractControl

  constructor(
    public form: FormGroupDirective
  ) { }

  ngOnInit(): void {  
  }

}
