import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumsSearchComponent } from './views/albums-search/albums-search.component';
import { SyncSearchComponent } from './views/sync-search/sync-search.component';


const routes: Routes = [
  {
    path: '',
    component: AlbumsSearchComponent
  },
  {
    path: 'sync',
    component: SyncSearchComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
