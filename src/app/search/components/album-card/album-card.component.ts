import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { Album } from 'src/app/core/model/search';

@Component({
  selector: 'app-album-card,[app-album-card]',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss']
})
export class AlbumCardComponent implements OnInit {

  @Input()
  album!:Album

  // @HostBinding('class.card')
  // class = true

  constructor() { }

  ngOnInit(): void {
  }

}
