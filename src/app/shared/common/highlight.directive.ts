import { Directive, ElementRef, Attribute, Input, OnChanges, SimpleChanges, Renderer2, HostBinding, HostListener, DoCheck } from '@angular/core';
import { PlaylistItemComponent } from 'src/app/playlists/components/playlist-item/playlist-item.component';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnChanges, DoCheck {

  @Input('appHighlight')
  color!: string

  @HostBinding('style.border-color')
  @HostBinding('style.color')
  // activeColor = 'red'
  get activeColor() {
    return this.hover ? this.color : ''
  }

  @HostBinding('class.active')
  hover = false

  // toggle(event:MouseEvent){
  // toggle(type: MouseEvent['type']) {
  @HostListener('mouseenter', ['$event.type'])
  activate() {
    this.hover = true
    // this.activeColor = this.color
  }

  @HostListener('mouseleave', ['$event.type'])
  deactivate() {
    this.hover = false
    // this.activeColor = ''
  }

  constructor(
    // @Attribute('color') private color: string,
    // private item: PlaylistItemComponent,
    private elem: ElementRef<HTMLElement>,
    private renderer: Renderer2) {
    // console.log(item)
  }

  ngDoCheck() {
    // this.activeColor = this.hover ? this.color : ''
  }

  ngOnChanges(changes: SimpleChanges): void {
    // this.activeColor = this.hover ? this.color : ''
    // changes['color'].currentValue;
    // this.elem.nativeElement.style.color = this.color
    // this.renderer.setStyle(this.elem.nativeElement, 'borderColor', this.color)
    // this.elem.nativeElement.addEventListener()
    // this.elem.nativeElement.removeEventListener()
    // const unlisten = this.renderer.listen(document.body,'mouseenter',(event)=>{})
  }

  ngOnInit() {
    console.log('HEllo appHighlight', this.color)
  }


  ngOnDestroy() {
    console.log('Bye bye appHighlight')
  }
}

// console.log(HighlightDirective)