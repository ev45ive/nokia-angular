import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-playlist-item',
  templateUrl: './playlist-item.component.html',
  styleUrls: ['./playlist-item.component.scss']
})
export class PlaylistItemComponent implements OnInit {

  // @HostBinding('class.list-group-item')
  // @HostBinding('class.playlist-color')
  // class = true;

  @HostBinding('class')
  class= 'list-group-item playlist-color'

  constructor() { }

  ngOnInit(): void {
  }

}
