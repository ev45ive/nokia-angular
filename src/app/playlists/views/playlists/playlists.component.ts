import { Component, OnInit } from '@angular/core';
import { NgIf } from '@angular/common';
import { Playlist } from 'src/app/core/model/playlist';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent {

  selected = this.playlistsService.selectedChanges
  playlists = this.playlistsService.playlistsChanges


  selectPlaylistById(playlistId: Playlist['id']) {
    if (playlistId) {
      this.router.navigate(['/playlists', playlistId])
    } else {
      this.router.navigate(['/playlists'])
    }
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService,
  ) {
    route.paramMap.subscribe(paramMap => {
      const playlistId = paramMap.get('playlist_id') || null
      this.playlistsService.selectPlaylistById(playlistId)
    })
  }

  ngOnInit(){
    this.playlistsService.fetchMyPlaylists()
  }

}
