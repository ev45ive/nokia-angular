import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YesnoPipe } from './common/yesno.pipe';
import { FormsModule } from '@angular/forms';
import { HighlightDirective } from './common/highlight.directive';
import { UnlessDirective } from './common/unless.directive';
import { ErrorMessagesComponent } from './forms/error-messages/error-messages.component'



@NgModule({
  declarations: [YesnoPipe, HighlightDirective, UnlessDirective, ErrorMessagesComponent],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    YesnoPipe, 
    FormsModule, HighlightDirective, UnlessDirective, ErrorMessagesComponent
  ]
})
export class SharedModule { }
