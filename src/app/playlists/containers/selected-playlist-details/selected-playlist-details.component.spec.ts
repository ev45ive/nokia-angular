import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedPlaylistDetailsComponent } from './selected-playlist-details.component';

describe('SelectedPlaylistDetailsComponent', () => {
  let component: SelectedPlaylistDetailsComponent;
  let fixture: ComponentFixture<SelectedPlaylistDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedPlaylistDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedPlaylistDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
