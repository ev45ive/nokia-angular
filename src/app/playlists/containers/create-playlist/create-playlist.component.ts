import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-create-playlist',
  templateUrl: './create-playlist.component.html',
  styleUrls: ['./create-playlist.component.scss']
})
export class CreatePlaylistComponent implements OnInit {

  playlist: Partial<Playlist> = {
    name: '', favorite: false, color: '#ff00ff', public: false
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService) {
  }

  cancel() { }
  save(draft: Partial<Playlist>) {
    this.playlistsService.createPlaylist({
      ...this.playlist,
      ...draft
    })
  }

  ngOnInit(): void {
  }

}
